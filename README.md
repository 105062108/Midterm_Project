# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. leave comment under any post
* Other functions (add/delete)
    1. 設定頭像
    2. 修改密碼
    3. 修改暱稱

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
***
## login.js
* 登入頁面
* 用 Lab6 的 code 來做登入、註冊、`google`登入功能。
* 最底下`Nickname()`處，額外實作了設定暱稱的功能，使用信箱註冊時可以設定自己的名字。
* (使用 google 登入則 default 成使用者在 google 帳號中的名字。)
* (用了`firebase.auth()`內建的`currentUser`，以及裡面的`updateProfile`來更改`displayName`。)
* 其他部分則跟 Lab6 提供的 code 一樣。

## home.js
* 主畫面。
* 可以點閱任何一篇文章，或者發布新貼文。
* 發文及載入貼文的部分，使用助教在 Lab6 提供的 function 與演算法。
* 每篇貼文底部會顯示作者的大頭貼、Email、以及發布此貼文的時間。
* 使用了`unshift`使得載入貼文時會依照發布的先後順序來載入。
* 顯示作者的部分，使用`childData.email`取得發布者的 email，大頭貼則是從`childData.url`取得，兩者在發布時就已經在`set`中寫入 database。
* 顯示時間的部分，使用在`time.js`寫好的 function 來取得當前時間。
* 最上方使用了 Lab6 時實作的登出功能，除了登出後跳轉到登入畫面外其餘都一樣。
* 最下方加了`google notification`的顯示要求。

## post.js
* 貼文頁面。
* 會顯示該篇貼文的標題、內文、發布者、以及留言，每篇留言左方會顯示使用者設定的大頭貼。
* 可以直接在下方留言。
* 顯示標題、內文、發布者是透過更改html檔中`cur_title`、`cur_content`、`author`的 innerHTML 來實作。
* 留言部分使用了`unshift`使得載入留言時會依照發布的先後順序來載入。
* 留言顯示的大頭貼是從發布時寫入的`userImage`來取得。
* 發布留言的部分與發布貼文使用的 function 一致。

## account.js
* 使用者頁面。
* 會顯示使用者的 email 以及暱稱，並且提供修改暱稱的功能。
* 此外也可以設定大頭貼、重設密碼(以電子郵件註冊者適用)。
* 先檢查使用者是否已經登入，若是，透過更改 html 檔中`myEmail`與`myName`的 innerhtml 來顯示使用者的信箱與暱稱，若否則跳轉回登入頁面。
* 修改暱稱的部分，與`login.js`中使用的方法相同。
* 設定大頭貼使用了`storage`的上傳、下載圖片功能，以及`FileReader`、`readAsDataURL`、`getDownloadURL`與`updateProfile`。
* 修改密碼使用了`firebase.auth`中的`sendPasswordResetEmail`來實作。

## develop.js
* 開發者頁面。
* 實作了`google notification`的功能。
* 點擊"開發者推薦"按鈕，會激活`notify()`這個 function。
* 在`notify`中，會 new 一個`Notification`，並且將設定好的標題、內容與連結顯示出來。
* (點擊此通知會打開一個 youtube 頁面，可以聽到開發者喜歡的一首歌。)
* 總之就是使用了`Notification`。

## time.js
* 由於直接 new 一個`Date`不太美觀(後面會有 "台北標準時間"，很醜)，所以寫了這個 js 檔。
* 有6個 function，分別會回傳排版過的 (前面會視情況補 0 的) 年、月、日、時、分、秒。
* 分別使用了`Date`中的`getFullYear`、`getMonth`、`getDate`、`getHours`、`getMinutes`、`getSeconds`來取得不同元素。

## 一些額外的東西
* 在開發者頁面中，做了一個連結可以跳轉到作業一的`canvas`去畫畫，可以畫一畫再點連結回論壇。
* 在上方選單列有一個`四季廢話`的頁面，點進去可以看到開發者花了許久蒐羅的廢話集錦。

## Security Report (Optional)
* 安全性
    * 只有已登入的使用者才看得到貼文以及留言。
    * 開發者不會取得使用者的密碼，且有實作修改密碼的功能。

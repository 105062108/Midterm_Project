document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notify() {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('鮪魚', {
            icon: 'https://upload-images.jianshu.io/upload_images/2193164-79c32531d51efd39',
            body: "聽個音樂吧!",
        });
        notification.onclick = function () {
            window.open("https://www.youtube.com/watch?v=0J5zEI7aAe4");
        };
    }
}
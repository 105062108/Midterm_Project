function init() {
    var _mail = document.getElementById("email");
    var _pswd = document.getElementById("password");
    var _login = document.getElementById("loginbtn");
    var _register = document.getElementById("registerbtn");
    var _google = document.getElementById("googlebtn");

    _register.addEventListener('click', e => {
        var email = _mail.value;
        var password = _pswd.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
            Nickname();
            alert("註冊成功!");
        }).catch(e => console.log(e.message));
        _mail.value = '';
        _pswd.value = '';
    });

    _login.addEventListener('click', e => {
        var email = _mail.value;
        var password = _pswd.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
            window.location.href = "home.html";
        }).catch(e => console.log(e.message));
        _mail.value = '';
        _pswd.value = '';
    });

    var provider = new firebase.auth.GoogleAuthProvider();
    _google.addEventListener('click', e => {
        console.log('signInWithPopup');
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "home.html";
        }).catch(function (error) {
            console.log('error: ' + error.message);
        });
    });
}

window.onload = function () {
    init();
};

function Nickname(){
    var user = firebase.auth().currentUser; 
    var nick = document.getElementById('nick');
    user.updateProfile({
        displayName : nick.value,
    });
}
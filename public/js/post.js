function post() {
    var theTitle = document.getElementById('cur_title');
    var theContent = document.getElementById('cur_content');
    var theAuthor = document.getElementById('author');
    var getKey = location.href.split('?');
    var theRealKey = getKey[1];
    var theRealKeyRef = "com_list/" + theRealKey;
    var user_email = '';
    var user_image_url = '';
    var getNickname = '';
    var getAuthor = '';
    var AuthorHead = '發布者: ';
    console.log(theRealKeyRef);
    var ref = firebase.database().ref(theRealKeyRef);
    ref.on('value', getData, errData);

    function getData(data) {
        theTitle.innerHTML = data.val().title.fontsize("5").fontcolor("blue");
        theContent.innerHTML = data.val().data.fontsize("3");
        theContent.style.wordWrap = 'break-word';
        theAuthor.innerHTML = AuthorHead.fontsize("3").fontcolor("brown") + data.val().name.fontsize("3").fontcolor("blue")
            + ' (' + data.val().email + ')';
    }

    function errData(data) {
        location.href = "home.html";
    }

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('option');
        // Check user login
        if (user) {
            user_email = user.email;
            getNickname = user.displayName;
            user_image_url = user.photoURL;
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                    alert("登出成功!");
                    window.location.href = "index.html";
                }).catch(function () {
                    alert("oops! logout failed!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    var com_btn = document.getElementById('commentbtn');
    var com_txt = document.getElementById('mycom');
    var says = ': ';

    com_btn.addEventListener('click', function () {
        if (com_txt.value != "") {
            var comsRef = firebase.database().ref(theRealKeyRef + '/comment');
            var newRef = comsRef.push();
            newRef.set({
                // ...
                email: user_email,
                data: getNickname.fontcolor("blue") + says.fontcolor("blue") + com_txt.value,
                year: yr(),
                month: mt(),
                day: dt(),
                hour: hr(),
                minute: min(),
                second: sec(),
                userImage: user_image_url
            });
            com_txt.value = "";
        }
    });

    var com_a = "<div><img src='images/capoo.png' style='height: 50px; width: 50px; border-radius: 2px; padding: 5px;'>";
    var com_b = "</div><div>";
    var com_c = "</div>";
    var theKey;
    function go() {
        window.location.href = "index.html";
    }
    var formatting = "作者：";
    var by = formatting.fontcolor("darkblue");
    var atTime = " 時間：";
    var at = atTime.fontcolor("darkblue");
    // List for store posts html
    var total_com = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    var comsRef = firebase.database().ref(theRealKeyRef + '/comment');
    comsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var author = firebase.auth().currentUser;
                var childData = childSnapshot.val();
                var email_col = childData.email.fontcolor("blue");
                theKey = childSnapshot.key;
                com_a = "<div><img src='" + childData.userImage + "' style='height: 50px; width: 50px; border-radius: 2px; padding: 5px;'>";
                total_com.unshift(com_a + childData.data.fontsize("3") + com_b + com_c  + at +
                    childData.year + '.' + childData.month + '.' + childData.day + ' ' +
                    childData.hour + ":" + childData.minute + ':' + childData.second);
                first_count += 1;
            })
            document.getElementById('comments').innerHTML = total_com.join('');

            comsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var email_col = childData.email.fontcolor("blue");
                    theKey = data.key;
                    com_a = "<div><img src='" + childData.userImage + "' style='height: 50px; width: 50px; border-radius: 2px; padding: 5px;'>";
                    total_com.unshift(com_a + childData.data.fontsize("3") + com_b + com_c +
                        at + yr() + '.' + mt() + '.' + dt() + ' ' + hr() + ":" + min() + ':' + sec());
                    document.getElementById('comments').innerHTML = total_com.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    post();
};
var getMail = '';
function set() {
    firebase.auth().onAuthStateChanged(function (user) {
        var user_email = '';
        // Check user login
        if (user) {
            user_email = user.email;
            getMail = user.email;
            var myMail = document.getElementById('myEmail');
            var myName = document.getElementById('myName');
            myMail.innerHTML = user_email;
            myName.innerHTML = user.displayName;
        } else {
            alert("您尚未登入!!");
            window.location.href = "index.html";
        }
    });
}

window.onload = function () {
    set();
}

function rstpswd() {
    var auth = firebase.auth();
    auth.sendPasswordResetEmail(getMail).then(function () {
        alert("已發送信件!");
        window.location.reload();
    }).catch(function (error) {
        alert("oops! 有地方出錯了!");
    });
}    

var newName = document.getElementById('newName');

function Nickname() {
    var user = firebase.auth().currentUser;
    if (newName.value != '') {
        user.updateProfile({
            displayName: newName.value,
        });
        alert("設定成功!!");
    }
    else {
        alert("暱稱不能為空白!!");
    }
    window.location.reload();
}

var loadFile = function (event) {
    var reader = new FileReader();
    reader.onload = function () {
        var output = document.getElementById('output');
        output.src = reader.result;

    };
    reader.readAsDataURL(event.target.files[0]); // preview picture
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var storageRef = firebase.storage().ref();
            var fileName = user.email + "userImage";
            storageRef.child(fileName).put(event.target.files[0]).then(function (snapshot) {
                console.log('uploaded success');
            })
            storageRef.child(fileName).getDownloadURL().then(function (url) {
                user.updateProfile({
                    photoURL: url
                }).then(function () {
                    console.log(user.photoURL);
                    alert("上傳成功!");
                    window.location.reload();
                }, function (error) {
                    console.log(error);
                });
            });
        } else {
            alert('您尚未登入!');
            window.location.href = "index.html";
        }
    });
};
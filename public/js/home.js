document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function init() {
    var user_email = '';
    var user_image_url = '';
    var user_name = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('option');
        // Check user login
        if (user) {
            user_email = user.email;
            user_image_url = user.photoURL; 
            user_name = user.displayName;
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', e => {
                firebase.auth().signOut().then(function () {
                    alert("登出成功!");
                    window.location.href = "index.html";
                }).catch(function () {
                    alert("oops! logout failed!");
                });
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('postbtn');
    post_txt = document.getElementById('comment');
    post_title = document.getElementById('postTitle');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var postsRef = firebase.database().ref('com_list');
            var newPostRef = postsRef.push();
            newPostRef.set({
                // ...
                email: user_email,
                title: post_title.value,
                data: post_txt.value,
                year: yr(),
                month: mt(),
                day: dt(),
                hour: hr(),
                minute: min(),
                second: sec(),
                comment: '',
                url: user_image_url,
                name: user_name
            });
            post_txt.value = "";
        }
    });

    var post_a = "<div><h4 style='color: darkblue'>➮近期發布</h4><img src='images/capoo.png' style='height: 50px; width: 50px; border-radius: 2px; padding: 5px;'><a href='home.html?" + theKey + "'>";
    var post_b = "</a></div><div>";
    var post_c = "</div>";
    var theKey;
    function go() {
        window.location.href = "index.html";
    }
    var formatting = "作者：";
    var by = formatting.fontcolor("darkblue");
    var atTime = " 時間：";
    var at = atTime.fontcolor("darkblue");

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var email_col = childData.email.fontcolor("purple");
                theKey = childSnapshot.key;
                post_a = "<div><h4 style='color: darkblue'>➮近期發布</h4><img src='" + childData.url + "'style='height: 50px; width: 50px; border-radius: 2px; padding: 5px;'><a href='post.html?" + theKey + "'>";
                total_post.unshift(post_a + childData.title.fontsize("4").fontcolor("blue") + post_b + by + email_col + post_c + at +
                    childData.year + '.' + childData.month + '.' + childData.day + ' ' +
                    childData.hour + ":" + childData.minute + ':' + childData.second);
                first_count += 1;
            })
            document.getElementById('posts').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var email_col = childData.email.fontcolor("purple");
                    theKey = data.key;
                    post_a = "<div><h4 style='color: darkblue'>➮近期發布</h4><img src='" + childData.url + "'style='height: 50px; width: 50px; border-radius: 2px; padding: 5px;'><a href='post.html?" + theKey + "'>";
                    total_post[total_post.length] = post_a + childData.title.fontsize("4").fontcolor("blue") + post_b + by + email_col + post_c +
                        at + yr() + '.' + mt() + '.' + dt() + ' ' + hr() + ":" + min() + ':' + sec();
                    document.getElementById('posts').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};

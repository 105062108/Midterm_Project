// Get time
function yr () {
    var dt = new Date();
    return dt.getFullYear();
}
function mt () {
    var dt = new Date();
    var realmt = dt.getMonth() + 1;
    if(realmt < 10) return '0' + realmt;
    else return realmt;
}
function dt () {
    var dt = new Date();
    var realdt = dt.getDate();
    if(realdt < 10) return '0' + realdt;
    else return realdt;
}
function hr () {
    var dt = new Date();
    var realhr = dt.getHours();
    if(realhr < 10) return '0' + realhr;
    else return realhr;
}
function min () {
    var dt = new Date();
    var realmin = dt.getMinutes();
    if(realmin < 10) return '0' + realmin;
    else return realmin;
}
function sec () {
    var dt = new Date();
    var realsec = dt.getSeconds();
    if(realsec < 10) return '0' + realsec;
    else return realsec;
}